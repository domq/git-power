#!/usr/bin/env perl
#
# Copyright 2018 École Polytechnique Fédérale de Lausanne (EPFL). All
# Rights Reserved.
#
# Author: Dominique Quatravaux <dominique@quatravaux.org>
#
# All the power of git in a rebase-mostly,
# never-let-a-code-review-stall-you-again workflow.
#
# Goes well with this in ~/.gitconfig:
#
# [branch]
#    autosetupmerge = true
#    autosetuprebase = interactive
# [rebase]
#	autosquash = true
# [sequence]
#    editor = git power rebase-editor

use strict;
use warnings;
use v5.21;

use Getopt::Long;

use IPC::System::Simple;  # For autodie qw(system exec)
use autodie qw(:all);
use IO::All;
use Carp qw(confess);

sub usage {
  my $errcode = @_ ? shift : 2;
  STDERR->print(<<'EOF');
Usage: git power [subcommand] [...]

Available subcommands:

   rebase-editor       Edit the rebase todo in a powerful way

EOF
  exit 2;
}

main();

exit(0); ##################################################################


sub main {
  Getopt::Long::Configure("pass_through");

  dispatch();
  exit 0;
}

sub dispatch {
  usage() unless @ARGV >= 1;
  (my $cmd = "subcommand_" . shift(@ARGV)) =~ s/-/_/g;
  no strict "refs";
  usage() unless (defined &$cmd);
  $cmd->();
}

sub running_for_real {
  return !! $ENV{GIT_CHERRY_PICK_HELP};
}

sub subcommand_rebase_editor {
  GetOptions() or usage;
  usage unless @ARGV == 1;
  my $filename = $ARGV[0];

  if (my ($dir) = $filename =~ m|^(.*)/.git/|) {
    chdir($dir);
  }

  ## Uncomment the following line if you want to run
  ## subcommand_rebase_editor under the Perl debugger:
  # warn("Holding for debug..."), system("sleep 86400") if running_for_real;

  my $todo = new RebaseTodo(io->file($filename)->slurp);

  if (! running_for_real) {
    $todo->print(io->stdout);
    exit 0;
  }

  my $new_rebase_todo_file = io("${filename}-NEW");
  unless (
    $todo->print($new_rebase_todo_file) &&
      $new_rebase_todo_file->close() &&
      $new_rebase_todo_file->rename($filename) )
    {
      confess "Could not rename $filename-NEW to $filename: $!";
    }

  exec_editor($filename);
}

sub exec_editor {
  my $editor = $ENV{"GIT_EDITOR"} || $ENV{"EDITOR"};
  exec ("$editor " . join(" ", @_));
}

package RebaseTodo;  #####################################################

use strict;
use Carp qw(confess);
use Proc::ProcessTable;
use YAML;
use IO::All;
use Graph::Directed;
use Graph::TransitiveClosure;
use Set::Scalar;

sub _zero_or_one {
  my (@things) = @_;
  confess unless @things <= 1;
  return $things[0];
}

sub _the_one {
  my (@things) = @_;
  confess unless @things == 1;
  return $things[0];
}

sub new {
  my $class = shift;

  my ($from, $to, $onto, @commands, $help);
  for(local $_ = shift; ; $_ = shift) {
    last if ($from, $to, $onto) = m/
               ^ [#] \Q Rebase \E
                     ([0-9a-f]+) \Q..\E ([0-9a-f]+)
                             \Q onto \E ([0-9a-f]+)
                     .* \n $/x;

    push @commands, $_ if m/./;
  }
  confess "Banner not found" unless defined $to;

  $help = join("", @_);


  my $self = bless {
    help        => $help,
    from        => $from,
    to          => $to,
    onto        => $onto,
    rebase_args => [$class->_parse_git_rebase_cmdline]
  }, $class;

  $self->{steps} = [$self->streamline(@commands)];

  return $self;
}

sub print {
  my ($self, $fd) = @_;

  local $_; $fd->println($_) for @{$self->{steps}};
  $fd->print("\n");
  $fd->println("#" x 75);
  $fd->println("# " . $self->banner);
  $fd->println("#" x 75);
  $fd->print($self->{help});
}

sub streamline {
  my ($self, @raw_steps) = @_;

  my $graph = $self->_git_graph;

  # Weed out all rebase steps that "git rebase" picked out of the
  # wrong side(s) of merge commits. If a step is not a Git ancestor
  # of $self->{to}, drop it.
  foreach my $v ($graph->vertices) {
    next if $self->is_reachable($v, rev_parse($self->{from}));
    $graph->delete_vertex($v);
  }

  my @steps;
  foreach my $raw_step (@raw_steps) {
    my $step = parse RebaseTodo::Step($self, $raw_step);
    next unless $graph->has_vertex($step->sha);
    confess "Please don't --preserve-merges" if $step->is_merge_commit;
    push @steps, $step;
  }

  # Further delete the merge commits from the graph, so that the
  # (unsplit) "subway lines" appear as weakly connected components.
  $graph->delete_vertex($_) foreach ($self->merge_commits_set->members);
  my @subways = sort { $a->compare_by_ancestry($b) }
    (map { RebaseTodo::Subway->split_branches($self, _subway_chain($graph, @$_)) }
     ($graph->weakly_connected_components));

  # Now we want to reorder @steps according to @subways, except we
  # want to keep autosquashed steps clumped with their original "pick"
  # (even if that makes them hop from one branch to another; in fact,
  # doing that is basically the whole point of power-rebasing)
  my @steps_by_subway = map { [] } @subways;
  while(@steps) {
    push my @clump, shift(@steps);
    confess unless $clump[0]->command eq "pick";
    push(@clump, shift(@steps)) while
      (@steps && $steps[0]->command ne "pick");

    my $subway_id = _the_one(grep { $subways[$_]->has($clump[0]->sha) }
                               (0..$#subways));
    push @{$steps_by_subway[$subway_id]}, @clump;
  }

  my @retval;
  foreach my $i (0..$#subways) {
    push(@retval, $self->remerge_branch_steps($subways[$i]))
      if ($subways[$i]->is_merge_based);

    push(@retval, "\n") if @retval;

    push @retval,
      $subways[$i]->banner,
      @{$steps_by_subway[$i]};

    if ($i < $#steps_by_subway) {
      push @retval, $self->mark_done_branch_steps($subways[$i]);
    }
  }
  return @retval;
}

sub mark_done_branch_steps {
  my ($self, $subway) = @_;

  my $branch_name = $subway->branch->name;
  chomp(my $orig_sha = `git rev-parse $branch_name`);

  # We've been rebasing on a branch $from_branch that was not "ours",
  # so first we have to record progress.
  return <<"UPDATE_REF";

exec git update-ref -m "git-power: rebase complete on ancestor branch" refs/heads/$branch_name HEAD
# *ATTENTION*: The above change is destructive. Should you git rebase --abort
# after this step, you will also want to run
#
#     git branch -f $branch_name $orig_sha
UPDATE_REF
}

sub remerge_branch_steps {
  my ($self, $subway) = @_;

  my $branch_name = $subway->branch->name;

  my @branches_to_remerge;
  foreach my $to_merge ($self->walk_merged_branches($subway->base)) {
    if (! $self->commit_is_ancestor($to_merge, $self->{onto})) {
      push @branches_to_remerge, $to_merge;
    }
  }

  return <<"REMERGE_NOOP" if (! @branches_to_remerge);
# $branch_name no longer needs merges.

REMERGE_NOOP

  return <<"REMERGE";
# Re-merge parents of $branch_name
exec git merge ${\join(" ", @branches_to_remerge)}

REMERGE
}

# Discover which branches $merge_sha was merged from.
# Keep walking parent commits that are also merge commits,
# and include them in the result.
sub walk_merged_branches {
  my ($self, $merge_sha) = @_;

  confess unless $self->is_merge_commit($merge_sha);

  my $branches = new Set::Scalar;
  foreach my $parent ($self->parent_shas($merge_sha)) {
    if ($self->is_merge_commit($parent)) {
      $branches->insert($self->walk_merged_branches($parent));
    } else {
      $branches->insert($self->by_tip($parent)->name);
    }
  }
  return $branches->members;
}

sub banner {
  my ($self) = @_;
  return sprintf("Power rebase %s: %s..%s onto %s",
                 join(" ", @{$self->{rebase_args}}),
                 $self->{from}, $self->{to},
                 $self->{onto});
}

sub commit_is_ancestor {
  my ($self, $from, $to) = @_;

  # https://stackoverflow.com/a/13526591/435004
  `git merge-base --is-ancestor "$from" "$to"`;
  return $? == 0;
}

sub _subway_chain {
  my ($graph, @vertices) = @_;  # Not a method

  # Get the degenerate cases out of the way
  confess "Should not call _subway_chain with zero vertices" unless @vertices;
  return @vertices if scalar(@vertices) == 1;

  my $subgraph = new Graph::Directed;  # When all you have is a hammer...
  foreach my $v (@vertices) {
    next unless my $next = _zero_or_one $graph->successors($v);
    $subgraph->add_edge($v, $next);
  }

  my @the_tip = $subgraph->predecessorless_vertices();
  if (@the_tip == 0) {
    confess unless @vertices >= 2; # Should have been caught as a
                                   # degenerate case, above
    confess "Umm, really? The history has a cycle?";
  } elsif (@the_tip > 2) {
    confess "Unable to power-rebase a non-linearizable history - "
      . "The following sha's have a common ancestor within the rebase todo: "
      . join(" ", @the_tip);
  }

  my @retval;
  for(my ($at) = @the_tip;
      $at ;
      $at = _zero_or_one $subgraph->successors($at)) {
        unshift @retval, $at;
      }

  return @retval;  # Guaranteed fork-free, in rebase-todo order (tip last)
}

sub _parse_git_rebase_cmdline {
  return "FAKE" if ! main::running_for_real;
  my $proc = new Proc::ProcessTable(enable_ttys => 0);

  for (my ($p) = grep {$_->pid == getppid() } @{$proc->table};
       $p and $p->pid and ($p->pid != 1);
       ($p) = grep {$_->pid == $p->ppid }  @{$proc->table}
      ) {
        next unless my ($args) = $p->cmndline =~ m/git-rebase -i (.*)$/;
        return grep { /^[a-z0-9]{4}/ } (split m/ /, $args);
      }
  confess "Could not find git-rebase in process table";
}

sub _git_graph {
  my ($self) = @_;

  unless ($self->{_git_graph}) {
    my $graph = $self->{_git_graph} = new Graph::Directed;

    my ($from, $to) = map { rev_parse($_) } ($self->{from}, $self->{to});
    my $gitcmd = "git log --topo-order --format=format:\%H:\%P $from..$to";
    my $gitpipe = io->pipe("$gitcmd |");
    while(local $_ = $gitpipe->getline) {
      chomp;
      die "Bizarre line in output of $gitcmd: $_" unless
        my ($sha, $parents) = split m/:/;
      $graph->add_edge($sha, $_) for split(m/ /, $parents);
    }
  }

  return $self->{_git_graph}->copy();  # Caller may mutate
}

sub is_reachable {
  my ($self, $from, $to) = @_;

  $self->{_tc} ||= new Graph::TransitiveClosure($self->_git_graph);
  return $self->{_tc}->is_reachable($from, $to);
}

sub parent_sha { my $self = shift; _the_one($self->parent_shas(@_)); }

sub parent_shas {
  my ($self, $from) = @_;

  $self->{_parenting_graph} ||= $self->_git_graph;
  return $self->{_parenting_graph}->successors($from);
}

sub merge_commits_set {
  my ($self) = @_;

  unless ($self->{_merge_commits_set}) {
    my $graph = $self->_git_graph;
    $self->{_merge_commits_set} = new Set::Scalar;
    foreach my $v ($graph->vertices) {
      $self->{_merge_commits_set}->insert($v) if $graph->successors($v) > 1;
    }
  }
  return $self->{_merge_commits_set};
}

sub is_merge_commit {
  my ($self, $sha) = @_;
  return $self->merge_commits_set->contains($sha);
}

# Just a package-level function, not a method
sub rev_parse {
  my ($committish) = @_;
  state %revs;
  if (! exists $revs{$committish}) {
    chomp($revs{$committish} = `git rev-parse --verify $committish`);
  }
  return $revs{$committish};
}

package RebaseTodo::Step;

use Carp qw(confess);

sub parse {
  my ($class, $todo_obj, $line_as_text) = @_;

  my ($is_comment, $command, $shortsha, $logentry) = $line_as_text =~
        m/
           ^
           ([#][ ])?
           (pick|fixup|squash) [ ]
           ([0-9a-f]+) [ ]
           (.*)
           $
         /x;

  confess $line_as_text unless defined $shortsha;
  return bless {
    todo       => $todo_obj,
    is_comment => ($is_comment or ""),
    command    => $command,
    shortsha   => $shortsha,
    logentry   => $logentry,
  }, $class;
}

sub shortsha { shift->{shortsha} }
sub command  { shift->{command} }
sub sha { RebaseTodo::rev_parse(shift->{shortsha}) }

sub is_merge_commit {
  my ($self) = @_;
  return $self->{todo}->is_merge_commit($self->sha);
}

use overload '""' => 'stringify';

sub stringify {
  my ($self) = @_;

  return "[merge commit $self->{shortsha}]" if $self->is_merge_commit;
  return "$self->{is_comment}$self->{command} $self->{shortsha} $self->{logentry}";
}

package RebaseTodo::Subway;

use Carp qw(confess);
use Set::Scalar;

# Models a "subway line" in the graphical representation (git glog /
# gitk / gitg) - A simple segment of non-merge commits that follow
# each other in a straight line, and all belong to the same branch.

sub new {
  my ($class, $todo_obj, @members) = @_;

  return bless {
    todo    => $todo_obj,
    members => [map { RebaseTodo::rev_parse($_) } @members]
  }, $class;
}

# @members is an ordered segment of non-merge commits that may belong
# to multiple branches; split them per branch
sub split_branches {
  my ($class, $todo_obj, @members) = @_;

  my @subways;

  # Bin @members into subways, starting from the last one, and creating
  # a new subway whenever we find a tip in a different branch
  SUBWAY: while(@members) {
    unshift (my @this_subway, pop(@members));
    my $subway_tip = $this_subway[0];
    confess $subway_tip unless
      (my $current_branch = RebaseTodo::LocalBranch->by_tip($subway_tip));

    MEMBER: while(@members) {
      my $next_branch = RebaseTodo::LocalBranch->by_tip($members[$#members]);
      last MEMBER if $next_branch and ($next_branch != $current_branch);
      unshift @this_subway, pop(@members);
    }

    push @subways, $class->new($todo_obj, @this_subway);
  }

  return @subways;
}

sub tip {
  my ($self) = @_;
  return $self->{members}->[-1];
}

sub _root {
  my ($self) = @_;
  return $self->{members}->[0];
}

sub base {
  my ($self) = @_;
  return $self->{todo}->parent_sha($self->_root);
}

sub is_merge_based {
  my ($self) = @_;
  my ($base) = $self->{todo}->parent_shas($self->_root);
  return ($base && $self->{todo}->is_merge_commit($base));
}

sub has {
  my ($self, @what) = @_;
  $self->{_set} ||= Set::Scalar->new(@{$self->{members}});

  return $self->{_set}->has(@what);
}

sub branch {
  my ($self) = @_;
  return RebaseTodo::LocalBranch->by_tip($self->tip);
}

sub banner {
  my ($self) = @_;
  my $name = $self->branch->name;
  return "# Commits for branch $name\n";
}

sub compare_by_ancestry {
  my ($a, $b) = @_;

  my ($atip, $btip) = ($a->tip, $b->tip);
  my $todo = $a->{todo};
  confess unless $b->{todo} == $a->{todo};

  return
    $todo->is_reachable($atip, $btip) ?  1 :
    $todo->is_reachable($btip, $atip) ? -1 :
    confess "Unable to power rebase criss-crossing merges "
    . "(unrelated subways found in rebase history starting at $atip and $btip)";
}

package RebaseTodo::LocalBranch;

use Carp qw(confess);
use IO::All;

sub the {
  my ($class, $branch_name) = @_;
  state %cache;

  $cache{$branch_name} ||= bless { name => $branch_name }, $class;
}

sub name { shift->{name} }

sub _reflog_abridged {
  my ($self) = @_;
  my $gitcmd = "git reflog show --no-abbrev-commit --date=unix $self->{name}";
  local $_;

  return @{$self->{_reflog}} if $self->{_reflog};

  $self->{_reflog} = [];
  for (io->pipe("$gitcmd |")->slurp) {
    chomp;
    # Example of a line:
    # e25af....af0 domq/another-way-to-import@{1234567890}: rebase -i (finish): yadda yadda
    confess "Weird line in $gitcmd: $_" unless
      (my ($sha, $reflog_timestamp, $reason_kw) =
       m/
          ^   ([0-9a-f]+)     # $sha
          \Q \E
          \S+ \@\{ (\d+) \}   # $reflog_timestamp
          \Q: \E
          ([a-zA-Z-]+) \b            # $reason_kw
        /x);
    next unless $reason_kw =~ m/^(?:commit|rebase|cherry-pick|git-power)$/;
    push @{$self->{_reflog}}, {sha => $sha, timestamp => $reflog_timestamp};
  }

  return @{$self->{_reflog}};
}

# Returns a UNIX timestamp of when this branch's head was last at
# $sha, or undef if it never was (or if the corresponding
# reflog entries expired)
sub reflog_has {
  my ($self, $sha) = @_;

  foreach my $reflog_entry ($self->_reflog_abridged) {
    if ($sha eq $reflog_entry->{sha}) {
      return $reflog_entry->{timestamp};
    }
  }
  return;
}

# Find and return the local branch that is (or was) at $committish
# most recently. If no such branch can be found, return undef.
sub by_tip {
  my ($class, $committish) = @_;

  my $sha = RebaseTodo::rev_parse($committish);

  my %candidates;
  foreach my $branch ($class->all) {
    my $timestamp = $branch->reflog_has($sha);
    $candidates{$branch->name} = $timestamp if defined $timestamp;
  }
  if (! %candidates) {
    return;
  } elsif (keys(%candidates) == 1) {
    return $class->the((keys(%candidates))[0]);
  } else {
    warn("Wut? $committish appears to have been created in several branches: " .
           join(", ", keys %candidates));
    # However unlikely, this could happen if someone replays exactly the
    # same rebase todo from the same base point in two different branches
    # (e.g. if they changed their minds mid-way regarding what branch
    # should be rebased to)

    my @best_candidates = sort
      # by ascending reflog timestamp
      {$candidates{$a} <=> $candidates{$b}}
      (keys %candidates);
    return $class->the($best_candidates[0]);
  }
}

sub all {
  my ($class) = @_;
  state @all;
  if (! @all) {
    @all = map { chomp; $class->the($_) }
    (io->pipe("git for-each-ref --format='%(refname:short)' refs/heads/ |")
     ->slurp);
  }
  return @all;
}
